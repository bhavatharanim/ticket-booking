const express=require('express');
var mongoose = require('mongoose');
const bodyParser = require('body-parser');
require('dotenv/config');
const app=express();

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());


const tiketRoute=require('./routes/ticket');
const updateRoute=require('./routes/update');

app.use('/ticket',tiketRoute);
app.use('/update',updateRoute);

mongoose.connect(process.env.DB_CONNECTION, {useNewUrlParser: true, useUnifiedTopology: true}).then(()=>{
    console.log("MongoDB connected..");
});

var db = mongoose.connection;

db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.listen(3002);
