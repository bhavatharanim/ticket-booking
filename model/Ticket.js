const mongoose=require('mongoose');
const user=require('./UserInfo');

const TicketSchema=mongoose.Schema({
    seatNo:{
        type:Number,
        min:1,
        max:40,
        required:true
    },
    booked:{
        type:Boolean,
        default:false
    },
    passenger:{
        type:mongoose.Schema.Types.ObjectId,ref:'user'
    }
});

module.exports=mongoose.model('Tickets',TicketSchema);