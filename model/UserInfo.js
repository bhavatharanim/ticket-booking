const mongoose=require('mongoose');
//const unique=require('mongoose-unique-validator');

const UserSchema= new mongoose.Schema({
    name:String,
    phNo:{
        type:String,
        unique:true
    },
    email:{
        type:String,
        unique:true
    }
});
//UserSchema.plugin(unique);
module.exports=mongoose.model('Users',UserSchema);