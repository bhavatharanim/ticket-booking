const express= require('express');
const router=express.Router();
const Ticket=require('../model/Ticket');
const User=require('../model/UserInfo');
require('dotenv/config');


router.post('/booking',ticket, (req,res)=>{
    var arr=new Array();
    Ticket.find({booked:true},(err,data)=>{
             try{
                 console.log(data);
                if(data){
                 console.log(data.length);
                 for(var i=0;i<data.length;i++){
                    arr.push(data[i].seatNo);
                 }
                 let flag=0;
                 for(var i=0;i<arr.length;i++){
                     if(req.seatNo==arr[i]){
                         User.findOneAndDelete({_id:req.user}).then(data=>res.status(200).json(data))
                         .catch(err =>{
                             res.json({message:err});
                         })
                         res.json({message:"Seat you are looking for is already booked!"});
                         flag=1;
                         break;
                        }     
                 }
                 if(flag==0){
                    const ticket=new Ticket({
                        seatNo:req.seatNo,
                        booked:true,
                        passenger:req.user
                    })
                    try{
                        const savedTicket= ticket.save();
                        res.json({message:"Seat boked successfully"}); 
                    }catch(err){
                        res.json({message:err});
                    }
                 }
                }
                 else{
                    res.json({message:err});
                 }
            }catch(err){
                res.json({message:err});
            }
        });
});    
function ticket(req,res,next){
    var seat=req.seatNo=req.body.seatNo;
    const user=new User(
        req.body.passenger  
    );
    console.log(user);
    try{
        if(seat<1 || seat>40){
            res.json({message:"Seat number should be between 1 to 40"});
            return;
        }
        const savedUser=user.save();
        req.user=user._id;
        next();
    }catch(err){
        res.json({message:err});
    }
}
router.get('/closed',(req,res) => {
    Ticket.find({booked:true},(err,data)=>{
        if(data){
             try{
                //  //console.log(data.length);
                //  for(var i=0;i<data.length;i++){
                //  const user_id=data[i].passenger;
                //  User.findById(user_id).then(user=>{
                //      if(user){
                //         res.json({User:user,Ticket:data[i]});
                //         //res.json();
                //         console.log(user);
                //      }
                //      else{
                //          res.json({message:"Passenger id is not valid"});
                //      }
                //  });
                //  }
                res.json(data);
            }
             catch(err){
                 res.json({message:err})
             }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
        }
        if(err) res.json({message:err})
    })                                                                             
})
router.get('/open',(req,res) => {
    var arr=new Array();
    var a=new Array();
    Ticket.find({booked:true},(err,data)=>{
        if(data){
             try{
                 console.log(data.length);
                 for(var i=0;i<data.length;i++){
                    arr.push(data[i].seatNo);

                 }
                 console.log(arr);
                 for(var i=1;i<=40;i++){
                    var k=0;
                    for(var j=0;j<arr.length;j++){
                        if(i==arr[j]){
                            k=1;
                            break;
                        }
                    }
                    if(k==0){
                        a.push(i);
                    }
                 }
                 //console.log(a);
                 res.json({Seats_not_booked:a});
             }
             catch(err){
                 res.json({message:err});
             }
            }
            if(err){
                res.json({message:err});
            }
        });
});
router.get('/:id',async (req,res)=>{
    try{
        const ticketdata= await Ticket.findById(req.params.id);
        if(ticketdata){
            res.json({status:ticketdata.booked});
        }else{
            res.json({status:"Id is not available"});
        }
    }catch(err){
        res.json({message:err});
    }
});
router.get('/details/:id',async (req,res)=>{
    try{
        const ticketdata= await Ticket.findById(req.params.id);
        if(ticketdata){
            const user=User.findById(ticketdata.passenger, function(err,user){
                if(err) res.json({message:err})
                if(user) res.json(user);
            } )
            }
    }catch(err){
        res.json({message:err});
    }
})
router.post('/reset',(req,res)=>{
    if(req.body.user==process.env.USER && req.body.password==process.env.PASS){
        Ticket.deleteMany({},(err,data)=>{
            if(err){
                res.json({message:err})
            }
            if(data){
                User.deleteMany({},(err,data)=>{
                    if(err){
                        res.json({message:err})
                    }
                    if(data){
                        res.json({message:"Reset was successfull"})
                    }
                });
            }
        });
    }
});
module.exports=router;
